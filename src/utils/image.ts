export function captureVideo(video: HTMLVideoElement): string {
  const canvas = document.createElement("canvas");
  canvas.width = video.videoWidth;
  canvas.height = video.videoHeight;
  const canvasContext = canvas.getContext("2d");
  if (canvasContext) {
    canvasContext.drawImage(video, 0, 0);
  }
  return canvas.toDataURL('image/png');
}