import React, {createContext, useRef, useCallback, useState, useMemo, useContext} from 'react';

import {Timeline} from './components';
import {captureVideo} from './utils';
import {Video} from './types';

import './App.css';

const MainContext = createContext({isPlaying: false});
export const useMainStoreStore = () => {
  const store = useContext(MainContext)
  if (!store) {
      throw new Error('useMainStoreStore must be used within a MainContext.Provider')
  }
  return store
}

function App() {
  const [currentTime, setCurrentTime] = useState(0);
  const [isCropping, setCropping] = useState(false);
  const [isPlaying, setPlaying] = useState(false);
  const [videos, setVideo] = useState<Video[]>([]);
  
  const interval = useRef<NodeJS.Timeout | undefined>();
  const videoRef = useRef<HTMLVideoElement | null>(null);

  const handleAddVideo = useCallback((files: FileList) => {
    if (files[0] && !isCropping) {
      setCropping(true);
      const video = document.createElement('video')
      const fileURL = URL.createObjectURL(files[0])
      video.src = fileURL
  
      const handleLoadData = () => {
        const screenshots: string[] = [];
        const duration = Math.floor(video.duration || 0)
        let times = Math.floor(duration) * 4;
  
        const makeScreenshot = () => {
          if (times) {
            screenshots.push(captureVideo(video));
            video.currentTime += 0.25;
            times -= 1;
          } else {
            video.removeEventListener("timeupdate", makeScreenshot);
  
            setVideo(_videos => {
              const fullDuration = _videos.length ? _videos[_videos.length - 1].to : 0;
              return [
                ..._videos, 
                {
                  key: Math.random().toString(),
                  from: fullDuration, 
                  to: fullDuration + duration,
                  screenshots,
                  fileURL
                }
              ];
            })
  
            video.removeEventListener('loadeddata', handleLoadData);

            setCropping(false);
          }
        };
  
        if (video) {
          video.addEventListener("timeupdate", makeScreenshot);
  
          video.currentTime = 0;
        }
      }
  
      video.addEventListener('loadeddata', handleLoadData);
    }
  }, [isCropping]);
  
  const onUpdate = useCallback((key: string, offset: number) => {
    setVideo(_videos => {
      const array = _videos.map(video => {
        const diff = (video.to * 100) - (video.from * 100)
        return video.key === key ? {
          ...video, 
          from: Number((offset / 100).toFixed(2)), 
          to: Number(((offset + diff) / 100).toFixed(2))
        } : video
      });
      return array.sort((a, b) => a.from - b.from);
    });
  }, []);

  const play = useCallback(() => {
    if (!videos.length) {
      return;
    }
    setPlaying(true);
    if (interval.current) {
      clearInterval(interval.current)
    }
    if (videoRef.current && videoRef.current.src) {
      videoRef.current.src = '';
    }
    setCurrentTime(0);
    const fullDuration = (videos.length ? videos[videos.length - 1].to : 0) * 10;
    let iteration = 0;
    const sorted = [...videos].sort((a, b) => b.from - a.from);

    interval.current = setInterval(() => {
      setCurrentTime(time => {
        const currentTime = time + 0.1;
        const videoFile = sorted.find(({from, to}) => currentTime >= from && currentTime < to);
        if (videoFile) {
          if (videoRef.current && videoRef.current.src !== videoFile.fileURL) {
            videoRef.current.pause();
            videoRef.current.src = videoFile.fileURL;
            setTimeout(() => {
              if (videoRef.current) {
                videoRef.current.play();
              }
            }, 0);
          }
        } else {
          if (videoRef.current) {
            videoRef.current.src = '';
          }
        }
        return currentTime;
      });
      iteration += 1;
      if (interval.current && iteration === Math.round(fullDuration)) {
        clearInterval(interval.current)
        setPlaying(false)
      }
    }, 100);
  }, [videos]);

  const store = useMemo(() => ({isPlaying}), [isPlaying]);

  return ( 
    <div className="app">
      <video className="video" ref={videoRef} preload="auto" width="800" height="400" muted></video>
      <MainContext.Provider value={store}>
        <Timeline 
          currentTime={currentTime} 
          disabled={isCropping} 
          videos={videos} 
          onAddVideo={handleAddVideo} 
          onUpdate={onUpdate} 
        />
      </MainContext.Provider>
      <button disabled={videos.length === 0} className="primary-button" onClick={play}>Play</button>
    </div>
  );
}

export default App;
