export type Video = {
  key: string; 
  from: number; 
  to: number; 
  screenshots: string[];
  fileURL: string;
}