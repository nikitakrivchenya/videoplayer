import React, {memo, FunctionComponent, useCallback, Fragment, useMemo} from 'react';

import {TimelineElement} from './components';
import {Video} from '../../types';

import './style.css';

const handleDrag = (e: React.DragEvent<HTMLDivElement>) => {
  e.stopPropagation();
  e.preventDefault();
}

type Props = {
  videos: Video[];
  disabled: boolean;
  onAddVideo: (files: FileList) => void;
  onUpdate: (key: string, value: number) => void;
  currentTime: number;
}

export const Timeline: FunctionComponent<Props> = memo(({videos, disabled, currentTime, onAddVideo, onUpdate}) => {
  const handleDrop = useCallback((e: React.DragEvent<HTMLDivElement>) => {
    handleDrag(e);
  
    onAddVideo(e.dataTransfer.files);
  }, [onAddVideo]);

  const renderVideo = useCallback((video: Video, videoIndex: number) => (
    <TimelineElement videoIndex={videoIndex} key={video.key} video={video} onUpdate={onUpdate} />
  ), [onUpdate]);

  const styles = useMemo(() => ({left: `${currentTime * 100}px`}), [currentTime]);

  return (
    <Fragment>
    <div className="block" onDragOver={handleDrag} onDragEnter={handleDrag} onDrop={handleDrop} >
      <div style={styles} className="timeline" />
      {videos.map(renderVideo)}
    </div>
    {disabled && <p>Loading...</p>}
    </Fragment>
  );
});
