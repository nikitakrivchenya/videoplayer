import React, {memo, FunctionComponent, useCallback, useRef, RefObject, useMemo} from 'react';

import {Video} from '../../../../types';
import {useMainStoreStore} from '../../../../App';

import './style.css';

type Props = {
  videoIndex: number;
  video: Video;
  onUpdate: (key: string, value: number) => void;
} 

export const TimelineElement: FunctionComponent<Props> = memo(({
  videoIndex, 
  video: {key, from, to, screenshots}, 
  onUpdate
}) => {
  const {isPlaying} = useMainStoreStore();
  const block: RefObject<HTMLDivElement> = useRef(null);
  const screenX = useRef(0);
  const allowMove = useRef(false);

  const styles = useMemo(() => ({
    left: from * 100, 
    width: (to - from) * 100, 
    zIndex: videoIndex
  }), [from, to, videoIndex]);

  const imageStyles = useMemo(() => ({width: `${100 / screenshots.length}%`}), [screenshots.length]);

  const renderScreenshot = useCallback((screenshot: string) => (
    <img 
      style={imageStyles} 
      key={screenshot} 
      src={screenshot} 
      alt="screenshot" 
    />
  ), [imageStyles]);

  const mouseDownHandler = useCallback((e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    if (!isPlaying) {
      screenX.current = e.screenX
      allowMove.current = true;
    }
  }, [isPlaying]);

  const updateTimelineHandler = useCallback((e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    if (allowMove.current) {
      allowMove.current = false;
      const value = (from * 100) + (e.screenX - screenX.current)
      onUpdate(key, value < 0 ? 0 : value)
    }
  }, [from, key, onUpdate]);

  const onMouseMoveHandler = useCallback((e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    if (allowMove.current && block.current) {
      const value = (from * 100) + (e.screenX - screenX.current)
      block.current.style.left = `${(value < 0 ? 0 : value).toFixed(2)}px`
    }
  }, [from, block]);

  return (
    <div
      ref={block}
      onMouseDown={mouseDownHandler} 
      onMouseMove={onMouseMoveHandler} 
      onMouseLeave={updateTimelineHandler}
      onMouseUp={updateTimelineHandler} 
      className="timeline-element" 
      style={styles}
    >
      {screenshots.map(renderScreenshot)}
    </div>
  );
});
